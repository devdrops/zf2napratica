##= Introdução =
 
Código fonte desenvolvido durante o aprendizado do livro ZF2 Na Prática.
Detalhes sobre o livro em http://www.zfnapratica.com.br/
Detalhes sobre o curso em http://code-squad.com/curso/zf2-na-pratica
 
##= Requisitos =
 
PHP 5.4.7.
 
##= Instalação =
 
Utilize git clone.

##= More Information =
 
Twitter (@devdrops).