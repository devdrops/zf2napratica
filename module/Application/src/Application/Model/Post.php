<?php

namespace Application\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

class Post extends Entity
{
    /**
     * Table name.
     * @var string
     */
    protected $tableName = 'posts';
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var datetime
     */
    protected $post_date;
    
    /**
     * Filters settings
     * @return \Zend\InputFilter\InputFilter
     */
    public function getInputFilter ()
    {
        if (!$this->inputFilter)
        {
            
            $inputFilter  = new InputFilter();
            $factory      = new InputFactory();
            
            /**
             * Field: id
             */
            $inputFilter->add($factory->createInput(array(
                'name' => 'id',
                'required' => true,
                'filters' => array (
                    array('name' => 'Int')
                )
            )));
            
            /**
             * Field: title
             */
            $inputFilter->add($factory->createInput(array(
                'name' => 'title',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100
                        )
                    )
                )
            )));
            
            /**
             * Field: description
             */
            $inputFilter->add($factory->createInput(array(
                'name' => 'description',
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                )
            )));
            
            /**
             * Field: post_date
             */
            $inputFilter->add($factory->createInput(array(
                'name' => 'post_date',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                )
            )));
            
            $this->inputFilter = $inputFilter;
        }
        
        return $this->inputFilter;
    }
}