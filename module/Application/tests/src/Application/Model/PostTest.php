<?php

/**
 * @author Davi Marcondes Moreira <davi.marcondes.moreira@gmail.com>
 */

namespace Application\Model;

use Core\Test\ModelTestCase;
use Application\Model\Post;
use Zend\InputFilter\InputFilterInterface;

/**
 * @group Model
 */
class PostTest extends ModelTestCase
{
    public function testGetInputFilter ()
    {
        $post = new Post();
        $if = $post->getInputFilter();
        // Assertion test
        //errado
        //$this->instanceOf("Zend\InputFilter\InputFilter", $if);
        //correto
        $this->assertInstanceOf("Zend\InputFilter\InputFilter", $if);
        return $if;
    }
    
    /**
     * @depends testGetInputFilter
     */
    public function testInputFilterValid($if)
    {
        // Testing filters
        $this->assertEquals(4, $if->count());
        
        $this->assertTrue($if->has('id'));
        $this->assertTrue($if->has('title'));
        $this->assertTrue($if->has('description'));
        $this->assertTrue($if->has('post_date'));
    }
    
    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInputFilterInvalido()
    {
        // Test if filters are working fine
        $post = new Post();
        $post->title = 'O trecho padrão original de Lorem Ipsum, usado desde o 
                        século XVI, está reproduzido abaixo para os 
                        interessados. Seções 1.10.32 e 1.10.33 de "de Finibus 
                        Bonorum et Malorum" de Cicero também foram reproduzidas 
                        abaixo em sua forma exata original, acompanhada das 
                        versões para o inglês da tradução feita por H. Rackham 
                        em 1914.';
    }
    
    /**
     * Testing a valid insert.
     */
    public function testInsert()
    {
        $post = $this->addPost();
        
        $saved = $this->getTable('Application\Model\Post')->save($post);
        
        // Testing filters (StringTags and StringTrim)
        $this->assertEquals('A Apple compra a Coderockr', $saved->description);
        
        // Testing PK auto increment
        $this->assertEquals(1, $saved->id);
    }
    
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Input inválido: description = 
     */
    public function testInsertInvalido()
    {
        $post = new Post();
        $post->title = 'teste';
        $post->description = '';
        
        $saved = $this->getTable('Application\Model\Post')->save($post);
    }
    
    public function testUpdate ()
    {
        $tableGateway = $this->getTable('Application\Model\Post');
        $post = $this->addPost();
        
        $saved = $tableGateway->save($post);
        $id    = $saved->id;
        
        $this->assertEquals(1, $id);
        
        $post = $tableGateway->get($id);
        $this->assertEquals('Apple compra a Coderockr', $post->title);
        
        $post->title = 'Coderockr compra a Apple';
        $updated = $tableGateway->save($post);
        
        $post = $tableGateway->get($id);
        $this->assertEquals('Coderockr compra a Apple', $post->title);
    }
    
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row 1
     */
    public function testDelete()
    {
        $tableGateway = $this->getTable('Application\Model\Post');
        $post = $this->addPost();
        
        $saved = $tableGateway->save($post);
        $id = $saved->id;
        
        $deleted = $tableGateway->delete($id);
        $this->assertEquals(1, $deleted); // Rows affected
        
        $post = $tableGateway->get($id);
    }
    
    private function addPost ()
    {
        $post = new Post();
        
        $post->title = 'Apple compra a Coderockr';
        $post->description = 'A Apple compra a <b>Coderockr</b><br /> ';
        $post->post_date = date ('Y-m-d H:i:s');
        
        return $post;
    }
}
